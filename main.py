from insertionSort import insertionSort
import datetime

def printOutput(arr):
    for count, element in enumerate(arr, start=1):
        if count % 3 == 0:
            print(element)
        else:
            print(str(element) + ", ", end="")

def main():
    arrIdenticalData20000 = []
    arrIdenticalData40000 = []
    arrIdenticalData80000 = []
    arrIdenticalData160000 = []
    arrIdenticalData320000 = []
    arrRandomData20000 = []
    arrRandomData40000 = []
    arrRandomData80000 = []
    arrRandomData160000 = []
    arrRandomData320000 = []
    arrRandomSortedData20000 = []
    arrRandomSortedData40000 = []
    arrRandomSortedData80000 = []
    arrRandomSortedData160000 = []
    arrRandomSortedData320000 = []
    arrRandomReverseSortedData20000 = []
    arrRandomReverseSortedData40000 = []
    arrRandomReverseSortedData80000 = []
    arrRandomReverseSortedData160000 = []
    arrRandomReverseSortedData320000 = []

    with open("indenticalData20000.txt") as my_file:
        data = my_file.read()
        arrIdenticalData20000 = [int(i) for i in data.split()]

    with open("indenticalData40000.txt") as my_file:
        data = my_file.read()
        arrIdenticalData40000 = [int(i) for i in data.split()]

    with open("indenticalData80000.txt") as my_file:
        data = my_file.read()
        arrIdenticalData80000 = [int(i) for i in data.split()]

    with open("indenticalData160000.txt") as my_file:
        data = my_file.read()
        arrIdenticalData160000 = [int(i) for i in data.split()]

    with open("indenticalData320000.txt") as my_file:
        data = my_file.read()
        arrIdenticalData320000 = [int(i) for i in data.split()]

    with open("randomData20000.txt") as my_file:
        data = my_file.read()
        arrRandomData20000 = [int(i) for i in data.split()]

    with open("randomData40000.txt") as my_file:
        data = my_file.read()
        arrRandomData40000 = [int(i) for i in data.split()]

    with open("randomData80000.txt") as my_file:
        data = my_file.read()
        arrRandomData80000 = [int(i) for i in data.split()]

    with open("randomData160000.txt") as my_file:
        data = my_file.read()
        arrRandomData160000 = [int(i) for i in data.split()]

    with open("randomData320000.txt") as my_file:
        data = my_file.read()
        arrRandomData320000 = [int(i) for i in data.split()]

    with open("randomSortedData20000.txt") as my_file:
        data = my_file.read()
        arrRandomSortedData20000 = [int(i) for i in data.split()]

    with open("randomSortedData40000.txt") as my_file:
        data = my_file.read()
        arrRandomSortedData40000 = [int(i) for i in data.split()]

    with open("randomSortedData80000.txt") as my_file:
        data = my_file.read()
        arrRandomSortedData80000 = [int(i) for i in data.split()]

    with open("randomSortedData160000.txt") as my_file:
        data = my_file.read()
        arrRandomSortedData160000 = [int(i) for i in data.split()]

    with open("randomSortedData320000.txt") as my_file:
        data = my_file.read()
        arrRandomSortedData320000 = [int(i) for i in data.split()]

    with open("randomReverseSortedData20000.txt") as my_file:
        data = my_file.read()
        arrRandomReverseSortedData20000 = [int(i) for i in data.split()]

    with open("randomReverseSortedData40000.txt") as my_file:
        data = my_file.read()
        arrRandomReverseSortedData40000 = [int(i) for i in data.split()]

    with open("randomReverseSortedData80000.txt") as my_file:
        data = my_file.read()
        arrRandomReverseSortedData80000 = [int(i) for i in data.split()]

    with open("randomReverseSortedData160000.txt") as my_file:
        data = my_file.read()
        arrRandomReverseSortedData160000 = [int(i) for i in data.split()]

    with open("randomReverseSortedData320000.txt") as my_file:
        data = my_file.read()
        arrRandomReverseSortedData320000 = [int(i) for i in data.split()]

    ##### input: identicalData20000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrIdenticalData20000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrIdenticalData20000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on identical data (size=20,000)")
    # printOutput(arrIdenticalData20000)
    
    ##### input: identicalData40000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrIdenticalData40000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrIdenticalData40000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on identical data (size=40,000)")
    # printOutput(arrIdenticalData40000)
    
    ##### input: identicalData80000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrIdenticalData80000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrIdenticalData80000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on identical data (size=80,000)")
    # printOutput(arrIdenticalData80000)
    
    ##### input: identicalData160000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrIdenticalData160000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrIdenticalData160000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on identical data (size=160,000)")
    # printOutput(arrIdenticalData160000)
    
    ##### input: identicalData320000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrIdenticalData320000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrIdenticalData320000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on identical data (size=320,000)")
    # printOutput(arrIdenticalData320000)
    
    ##### input: randomData20000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomData20000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomData20000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random data (size=20,000)")
    # printOutput(arrRandomData20000)
    
    ##### input: randomData40000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomData40000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomData40000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random data (size=40,000)")
    # printOutput(arrRandomData40000)
    
    ##### input: randomData80000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomData80000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomData80000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random data (size=80,000)")
    # printOutput(arrRandomData80000)
    
    ##### input: randomData160000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomData160000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomData160000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random data (size=160,000)")
    # printOutput(arrRandomData160000)
    
    ##### input: randomData320000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomData320000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomData320000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random data (size=320,000)")
    # printOutput(arrRandomData320000)
    
    ##### input: randomSortedData20000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomSortedData20000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomSortedData20000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random sorted data (size=20,000)")
    # printOutput(arrRandomSortedData20000)
    
    ##### input: randomSortedData40000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomSortedData40000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomSortedData40000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random sorted data (size=40,000)")
    # printOutput(arrRandomSortedData40000)
    
    ##### input: randomSortedData80000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomSortedData80000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomSortedData80000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random sorted data (size=80,000)")
    # printOutput(arrRandomSortedData80000)
    
    ##### input: randomSortedData160000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomSortedData160000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomSortedData160000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random sorted data (size=160,000)")
    # printOutput(arrRandomSortedData160000)
    
    ##### input: randomSortedData320000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomSortedData320000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomSortedData320000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random sorted data (size=320,000)")
    # printOutput(arrRandomSortedData320000)
    
    
    ##### input: randomReverseSortedData20000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomReverseSortedData20000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomReverseSortedData20000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random reverse sorted data")
    # printOutput(arrRandomReverseSortedData20000)
    
    ##### input: randomReverseSortedData40000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomReverseSortedData40000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomReverseSortedData40000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random reverse sorted data")
    # printOutput(arrRandomReverseSortedData40000)
    
    ##### input: randomReverseSortedData80000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomReverseSortedData80000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomReverseSortedData80000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random reverse sorted data")
    # printOutput(arrRandomReverseSortedData80000)
    
    ##### input: randomReverseSortedData160000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomReverseSortedData160000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomReverseSortedData160000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random reverse sorted data")
    # printOutput(arrRandomReverseSortedData160000)
    
    ##### input: randomReverseSortedData320000.txt
    # start = datetime.datetime.now()
    # insertionSort(arrRandomReverseSortedData320000)
    # end = datetime.datetime.now()
    # diff = end - start
    # print("size of input: ", len(arrRandomReverseSortedData320000))
    # print('insertion sort took', diff.total_seconds() * 1000, "milliseconds to run on random reverse sorted data")
    # printOutput(arrRandomReverseSortedData320000)





if __name__ == "__main__":
    main()
