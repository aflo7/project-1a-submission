def insertionSort(arr):
    comparisons = 0
    for j in range(1, len(arr)):
        key = arr[j]

        i = j - 1
        while i >= 0 and arr[i] > key:
            comparisons +=1
            arr[i + 1] = arr[i]
            i -= 1
        arr[i + 1] = key
            
    print('There was', comparisons, "comparisons made")
    return arr
